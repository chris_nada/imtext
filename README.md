# imtext (fork of [ImMultiText](https://github.com/Lefty-Lex/ImMultiText))

imtext is **single** header stream style extention for [ImGui](https://github.com/ocornut/imgui) text function.

Changes from original:
+ actually working on all platforms that I'm able to test with (gcc in Debian + Win 10)
+ renamed from `ImMultitext` to `imtext` as CamelCase naming of namespaces, classes and especially files is reserved for Java, C# and webdesigners (yes, you got me, ImGui does it too, but at least they don't do it in filenames)
+ changed code-style from Microsoft 💩 C# style to my variant of Stroustrup and:
    + LF instead of CRLF 🙄
    + 4 spaces instead of tabs 🙄
+ removed Image support as it was relying on DirectX 💩
+ removed all the `inline`s, as these do nothing in a proper compiler

Example

![Example](https://i.postimg.cc/4NBvk8mV/image.png)

Usage
```cpp
	// assuming you have a function `getFont` for getting your ImGui::Font*
	imtext::text() << ImColor(255,0,255,255) << "First line " << imtext::endl
	 << ImColor(0,255,255,255) << getFont("Verdana", 18) << "Second line " 
	 << getFont("Verdana", 14) << imtext::endl << ImColor(255,0,0,255) << "Third line";
```

The above code without using this header file would typcally look like this below

```cpp
	ImGui::PushStyleColor(ImGuiCol_Text, ImColor(255, 0, 255, 255).Value);
	ImGui::Text("First line ");
	ImGui::PopStyleColor();
	ImGui::PushStyleColor(ImGuiCol_Text, ImColor(0, 255, 255, 255).Value);
	ImGui::PushFont(getFont("Verdana", 18));
	ImGui::Text("Second line ");
	ImGui::PopStyleColor();
	ImGui::PopFont();
	ImGui::PushFont(getFont("Verdana", 14));
	ImGui::PushStyleColor(ImGuiCol_Text, ImColor(255, 0, 0, 255).Value);
	ImGui::Text("Third line");
	ImGui::PopStyleColor();
	ImGui::PopFont();
```

## Features

 - Combine strings with integers, floats, doubles and more !
 - You can use multiple fonts and colors without any manual cleanup ,it pops everything itself.
 - Simple to modify and display the data you like with your own classes / structures.
 - ~~You can even display images along side your text!~~ Removed

## How to use
Make sure to include `imtext.h`

Simply use the << operator to append the functionality you like, options include:
```cpp
    << [color] /*supports ImColor and ImVec4*/;
    
    << [text] /*supports const char* and strings*/;
    
    << [number value] /*supports int, double, float and various more*/;
    
    << [font] /*supports ImFont**/;
    
    << imtext::endl /*creates a new line*/;
    
    << imtext::setPrecision(int) /*sets the precision for floating point numbers);
```
## discord 
Original Author: PEPPAPEEPO#3123
