#pragma once

#include "imgui.h"
#include <sstream>
#include <iomanip>

namespace imtext {

const std::string endl = "/n";

struct Precision {
    Precision(int precision) : value(precision) {};
    const int value;
};

Precision set_precision(int precision) {
    return Precision(precision);
}

class Stream {

public:

    ~Stream() {
        if (this->colorCount >= 1) ImGui::PopStyleColor(this->colorCount);
        if (this->fontCount >= 1)  ImGui::PopFont();
    }

    Stream& operator<<(const ImColor& color) {
        // we do this to prevent our colorcount ever going higher then 1, easier to keep track of
        if (this->colorCount >= 1) {
            ImGui::PopStyleColor(this->colorCount);
            this->colorCount = 0;
        }
        ImGui::PushStyleColor(ImGuiCol_Text, color.Value);
        this->colorCount++;
        return *this;
    }

    Stream& operator<<(const ImVec4& color) {
        // we do this to prevent our colorcount ever going higher then 1, easier to keep track of
        if (this->colorCount >= 1) {
            ImGui::PopStyleColor(this->colorCount);
            this->colorCount = 0;
        }
        ImGui::PushStyleColor(ImGuiCol_Text, color);
        this->colorCount++;
        return *this;
    }

    Stream& operator<<(ImFont* font) {
        // we do this to prevent our fontcount ever going higher then 1, easier to keep track of
        if (this->fontCount >= 1) {
            ImGui::PopFont();
            this->fontCount = 0;
        }
        ImGui::PushFont(font);
        this->fontCount++;
        return *this;
    }


    inline Stream& operator<<(Precision precision) {
        this->decimalPrecision = precision.value;
        return *this;
    }

    inline Stream& operator<<(const std::string& text) {
        if (text == endl) this->newLine();
        else {
            this->sameLine();
            ImGui::Text(text.c_str());
        }
        return *this;
    }

    inline Stream& operator<<(const char* text) {
        if (text == endl) this->newLine();
        else {
            this->sameLine();
            ImGui::Text(text);
        }
        return *this;
    }

    template<class T>
    inline Stream& operator<<(const T& value) {
        this->sameLine();
        if (this->decimalPrecision != -1 && (typeid(T) == typeid(float) || typeid(T) == typeid(double))) {
            std::ostringstream ss;
            ss << std::fixed <<  std::setprecision(this->decimalPrecision) << value;
            ImGui::Text(ss.str().c_str());
        }
        else ImGui::Text(std::to_string(value).c_str());
        return *this;
    }

private:

    int colorCount = 0;
    int fontCount = 0;
    int decimalPrecision = -1;

    void sameLine() {
        ImGui::SameLine();
    }

    void newLine() {
        ImGui::NewLine();
    }
};

// You can use this to auto call the destructor after all usages of the operator overload
Stream stream() {
    return Stream();
}

} // namespace imtext
